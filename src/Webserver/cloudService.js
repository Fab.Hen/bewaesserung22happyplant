// Autor: Fabiola Streitenberger
// Sendet die ueber MQTT empfangenen Daten weiter an ein Cloudanwendung

const mqtt = require("mqtt");
const {MQTTClient}  = require("./mqttHandler");
const {cloudServiceHandler} = require("./cloudServiceHandler");


// Quelle options:
// https://cedalo.com/blog/nodejs-mqtt-usage-with-mosquitto-example/
const options = {
    clientId: "userID",
    username: "username",
    password: "password"
};
const mqttUrl = "mqtt://localhost";
const mqttClient2 = mqtt.connect(mqttUrl, options);

const topics = ["area1", "area2", "area3", "temperature"];
const ws = 0;

const mqttClient = new MQTTClient(mqttClient2, cloudServiceHandler, topics, ws);
mqttClient.connect();