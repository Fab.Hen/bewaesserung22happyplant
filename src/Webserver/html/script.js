//Autor: Niklas Schaefer

// Quelle Grundlagen: https://www.youtube.com/watch?v=PkZNo7MFNFg
// Quelle Grundlagen: https://www.w3schools.com/js/default.asp

// WebSocket Verbindung aufbauen
// Quelle Websocket: 
//https://github.com/Vuka951/tutorial-code/blob/master/js-express-websockets/done/clients/index.html
const socket = new WebSocket("ws://localhost:3000");

// Verbindung geoeffnet
socket.addEventListener("open", function (event) {
    console.log("Connected to WS Server")
});

// Verfolgung ausgewaehlter Bereiche
let selectedImages = {};
// Nummer des zuletzt ausgewaehlten Bereichs
let selectedImageNumber = null;
// Zeitstempel des letzten Klicks
let lastClickTime = 0;
// Timeout fuer das Menue
let menuTimeout;
// E-Mail-Adresse fuer den Versand von Benachrichtigungen
let mailAddress = {
    emailAddress: "maxmustermann@gmailcom", // Hier sollte die E-Mail-Adresse eingesetzt werden
};

// Informationen zu Bewaesserungsmodi und Zeitplaenen
let modeInfo = {
    area1: "Winter", // Bewaesserungsmodus fuer Bereich 1
    area2: "Automatik", // Bewaesserungsmodus fuer Bereich 2
    area3: "Manuell", // Bewaesserungsmodus fuer Bereich 3
    automaticFrom1: "12:20", // Automatische Bewaesserungsstartzeit fuer Bereich 1
    automaticTo1: "15:30", // Automatische Bewaesserungsendzeit fuer Bereich 1
    automaticFrom2: "17:30", // Automatische Bewaesserungsstartzeit fuer Bereich 2
    automaticTo2: "18:40", // Automatische Bewaesserungsendzeit fuer Bereich 2
    automaticFrom3: "13:50", // Automatische Bewaesserungsstartzeit fuer Bereich 3
    automaticTo3: "16:45", // Automatische Bewaesserungsendzeit fuer Bereich 3
    manualArea1: "ON", // Manueller Bewaesserungsstatus fuer Bereich 1
    manualArea2: "OFF", // Manueller Bewaesserungsstatus fuer Bereich 2
    manualArea3: "ON", // Manueller Bewaesserungsstatus fuer Bereich 3
    errorIs: "false", // Fehlerstatus
};
// Informationen zur Temperatur

let temperatureVal = {
    temperatureVal: "22.22", // Gemessener Temperaturwert
};

// Event-Listener fuer das Oeffnen des Menues bei Klick auf den Button
document.getElementById("menuButton").addEventListener("click", function () {
    let menu = document.getElementById("menu");
    menu.classList.toggle("open"); 
});

// Funktion zum Loeschen des Menue-Timeouts
function clearMenuTimeout() {
    clearTimeout(menuTimeout);
}

// Funktion zum Schließen des Menues nach einer Verzoegerung
function closeMenu() {
    menuTimeout = setTimeout(function () {
        document.getElementById("menu").classList.remove("open");
    }, 500);
}

// Quelle: https://stackoverflow.com/questions/588040/window-onload-vs-document-onload
window.onload = function () {
    // Ueberpruefen der Bewaesserungsmodi-Informationen
    checkModeInfo();

    // Standardzeiten setzen
    setDefaultTimes(); 

    // Funktion zum Ueberpruefen der Bewaesserungsmodi-Informationen
    function checkModeInfo() {
        // Bewaesserungsbereich 1
        if (modeInfo.area1 === "Winter") {
            document.getElementById("imageField1").classList.add("selected-winter");
        } else if (modeInfo.area1 === "Automatik") {
            document.getElementById("imageField1").classList.add("selected-automatik");
        } else if (modeInfo.area1 === "Manuell" && modeInfo.manualArea1 === "ON") {
            document.getElementById("imageField1").classList.add("selected-manuell");
        }
        else if (modeInfo.area3 === "Manuell" && modeInfo.manualArea3 === "OFF") {
            document.getElementById("imageField3").classList.add("selected-manuelloff");
        }

        // Bewaesserungsbereich 2
        if (modeInfo.area2 === "Winter") {
            document.getElementById("imageField2").classList.add("selected-winter");
        } else if (modeInfo.area2 === "Automatik") {
            document.getElementById("imageField2").classList.add("selected-automatik");
        } else if (modeInfo.area2 === "Manuell" && modeInfo.manualArea2 === "ON") {
            document.getElementById("imageField2").classList.add("selected-manuell");
        }
        else if (modeInfo.area3 === "Manuell" && modeInfo.manualArea3 === "OFF") {
            document.getElementById("imageField3").classList.add("selected-manuelloff");
        }

        // Bewaesserungsbereich 3
        if (modeInfo.area3 === "Winter") {
            document.getElementById("imageField3").classList.add("selected-winter");
        } else if (modeInfo.area3 === "Automatik") {
            document.getElementById("imageField3").classList.add("selected-automatik");
        } else if (modeInfo.area3 === "Manuell" && modeInfo.manualArea3 === "ON") {
            document.getElementById("imageField3").classList.add("selected-manuell");
        }
        else if (modeInfo.area3 === "Manuell" && modeInfo.manualArea3 === "OFF") {
            document.getElementById("imageField3").classList.add("selected-manuelloff");
        }
    }
}

// Event-Listener fuer das Menue, um das Timeout zu loeschen, wenn die Maus darueber schwebt
document.getElementById("menu").addEventListener("mouseenter", clearMenuTimeout);
document.getElementById("menu").addEventListener("mouseleave", closeMenu);

// Funktion zur Behandlung von Klicks auf Bereiche
// Parameter: imageNumber (Bewaesserungsbereiche)
// Quelle: https://www.w3schools.com/jsref/event_onclick.asp
function handleImageClick(imageNumber) {
    let currentTime = new Date().getTime();
    // Ueberpruefen, ob der Klick innerhalb einer Sekunde nach dem vorherigen Klick erfolgt und dasselbe Bild ausgewaehlt ist
    if (currentTime - lastClickTime < 1000 && selectedImageNumber === imageNumber) {
//Phase 3 Zusatzaufgabe (teilweise bearbeitet): Individueller Bildupload aus Dateien in Bewaesserungsbereiche per Doppelklick
handleImageDoubleClick(imageNumber);
    } else {
        selectedImageNumber = imageNumber;
        lastClickTime = currentTime;
        // Einzelschritt: Menue fuer ausgewaehlten Bereich anzeigen
        showImageMenu();
    }
}

// Funktion zur Aktualisierung des manuellen Modus-Bereichsstatus
function updateManualModeAreaStatus(areaNumber, status) {
    const areaElement = document.getElementById(`imageField${areaNumber}`);
    if (status === 'OFF') {
        areaElement.classList.add('off');
    } else {
        areaElement.classList.remove('off');
    }
}

//Phase 3 Zusatzaufgabe (teilweise bearbeitet): Individueller Bildupload aus Dateien in Bewaesserungsbereiche per Doppelklick
function handleImageDoubleClick(imageNumber) {
    document.getElementById("fileInput").dataset.fieldNumber = imageNumber;
    document.getElementById("fileInput").click();
}

// Funktion zur Anzeige des Bereichmenues
function showImageMenu() {
    // Elemente abrufen
    let imageMenu = document.getElementById("imageMenu");
    let selectedImage = document.getElementById("imageField" + selectedImageNumber);
    // Position des Bereichmenues festlegen
    let rect = selectedImage.getBoundingClientRect();
    imageMenu.style.left = rect.left + rect.width / 2 + "px";
    imageMenu.style.top = rect.bottom + 10 + "px";
    imageMenu.style.display = "block"; // Menue anzeigen
    // Event-Listener hinzufuegen, um das Menue zu verstecken, wenn die Maus es verlaesst
    imageMenu.addEventListener("mouseleave", function () {
        hideImageMenu();
    });
}

// Funktion zum Verstecken des Bildmenues
function hideImageMenu() {
    document.getElementById("imageMenu").style.display = "none";
}

// Funktion zur Behandlung von Menuepunktklicks
// Parameter: option (Bewaesserungsmodus)
function handleMenuItemClick(option) {
    // Bildmenue verstecken
    hideImageMenu();
    // Ausgewaehltes Bild abrufen
    let selectedImage = document.getElementById("imageField" + selectedImageNumber);
    // Klassen fuer ausgewaehltes Bild entfernen
    selectedImage.classList.remove("selected-winter", "selected-automatik", "selected-manuell", "selected-manuelloff");
    // Logik je nach Menueoption
    if (option === "winter") {
        // Logik fuer Winter
        if (selectedImageNumber == 1) {
            modeInfo.area1 = "Winter";
        } else if (selectedImageNumber == 2) {
            modeInfo.area2 = "Winter";
        } else if (selectedImageNumber == 3) {
            modeInfo.area3 = "Winter";
        }
        console.log(modeInfo);
        sendMessage(); // Nachricht senden
        selectedImage.classList.add("selected-winter");
        hideAutomaticModeTextFields();
        updateManualModeSwitchesVisibility();
    } else if (option === "automatik") {
        // Logik fuer Automatik
        if (selectedImageNumber == 1) {
            modeInfo.area1 = "Automatik";
        } else if (selectedImageNumber == 2) {
            modeInfo.area2 = "Automatik";
        } else if (selectedImageNumber == 3) {
            modeInfo.area3 = "Automatik";
        }
        console.log(modeInfo);
        sendMessage(); 
        selectedImage.classList.add("selected-automatik");
        showAutomaticModeTextFields(selectedImageNumber);
        updateManualModeSwitchesVisibility();
    } else if (option === "manuell") {
        // Logik fuer Manuell
        if (selectedImageNumber == 1 && modeInfo.manualArea1 === "ON") {
            modeInfo.area1 = "Manuell";
            selectedImage.classList.add("selected-manuell");
        } else if (selectedImageNumber == 2 && modeInfo.manualArea2 === "ON") {
            modeInfo.area2 = "Manuell";
            selectedImage.classList.add("selected-manuell");
        } else if (selectedImageNumber == 3 && modeInfo.manualArea3 === "ON") {
            modeInfo.area3 = "Manuell";
            selectedImage.classList.add("selected-manuell");
        }
        else if (selectedImageNumber == 1 && modeInfo.manualArea1 === "OFF") {
            modeInfo.area1 = "Manuell";
            selectedImage.classList.add("selected-manuelloff");
        } else if (selectedImageNumber == 2 && modeInfo.manualArea2 === "OFF") {
            modeInfo.area2 = "Manuell";
            selectedImage.classList.add("selected-manuelloff");
        } else if (selectedImageNumber == 3 && modeInfo.manualArea3 === "OFF") {
            modeInfo.area3 = "Manuell";
            selectedImage.classList.add("selected-manuelloff");
        }
        console.log(modeInfo);
        sendMessage(); 
        hideAutomaticModeTextFields();
        showManualModeSwitches();
    } 
}

// Funktion zum Setzen der Standardwerte in die Textfelder im Automatikmodus
function setDefaultTimes() {
    const timeFields = Array.from({ length: 3 }, (_, index) => index + 1);
    timeFields.forEach((fieldNumber) => {
        const fromTime = document.getElementById(`fromTime${fieldNumber}`);
        const toTime = document.getElementById(`toTime${fieldNumber}`);
        // Ueberpruefen, ob die automatischen Zeiten in modeInfo vorhanden sind und setzen Sie die Standardwerte
        if (modeInfo[`automaticFrom${fieldNumber}`] && modeInfo[`automaticTo${fieldNumber}`]) {
            fromTime.value = modeInfo[`automaticFrom${fieldNumber}`];
            toTime.value = modeInfo[`automaticTo${fieldNumber}`];
        }
    });
}

// Funktion zum Anzeigen der Textfelder fuer die Automatik-Modus-Zeiteinstellungen
// Parameter: imageNumber (Bewaesserungsbereiche)
function showAutomaticModeTextFields(imageNumber) {
    const timeFields = Array.from({ length: 3 }, (_, index) => index + 1);
    // Event-Listener fuer die Textfelder und Anzeige entsprechend der Bildnummer aktualisieren
    timeFields.forEach((fieldNumber) => {
        const fromTime = document.getElementById(`fromTime${fieldNumber}`);
        const toTime = document.getElementById(`toTime${fieldNumber}`);
        // Event-Listener fuer Benutzereingaben 
        fromTime.addEventListener("keyup", (event) => handleUserInput(event, imageNumber, `automaticFrom${fieldNumber}`));
        toTime.addEventListener("keyup", (event) => handleUserInput(event, imageNumber, `automaticTo${fieldNumber}`));
        // Anzeige entsprechend der Bildnummer aktualisieren
        const displayValue = fieldNumber === imageNumber ? "block" : "none";
        [fromTime, toTime].forEach((element) => element.parentNode.style.display = displayValue);
    });
}

// Funktion zur Verarbeitung der Benutzereingabe
// Parameter: imageNumber (Bewaesserungsbereiche)
// Parameter: event (Enter-Druck)
// Parameter: fieldName (Feldname in Struct)
// Quelle: https://regexr.com/
function handleUserInput(event, imageNumber, fieldName) {
    if (event.key === "Enter") {
        const regex = /^([01]\d|2[0-3])\:[0-5]\d$/;
        const isValid = regex.test(event.target.value);
        const inputField = event.target;
        if (isValid) {
            console.log("The time is in valid format.");
            // Benutzereingabe in modeInfo speichern
            modeInfo[fieldName] = inputField.value;
            sendMessage();
            console.log(modeInfo);
            // Zuruecksetzen des roten Designs, wenn die Eingabe korrekt ist
            inputField.classList.remove("invalid-input");
        } else {
            console.log("The time is in invalid format.");
            // Hinzufuegen des roten Designs, wenn die Eingabe ungueltig ist
            inputField.classList.add("invalid-input");
        }
    }
}

// Funktion zum Anzeigen der automatischen Modus Textfelder ausblenden
function hideAutomaticModeTextFields() {
    const timeFields = Array.from({ length: 3 }, (_, index) => index + 1);
    // Anzeige der Textfelder auf "none" setzen
    timeFields.forEach((fieldNumber) => {
        const fromTime = document.getElementById(`fromTime${fieldNumber}`);
        const toTime = document.getElementById(`toTime${fieldNumber}`);
        [fromTime, toTime].forEach((element) => element.parentNode.style.display = "none");
    });
}

// Funktion zum Anzeigen der manuellen Modus Schalter
function showManualModeSwitches() {
    const manualSwitches = document.querySelector('.manualModeSwitches');
    manualSwitches.style.display = 'flex';
}
// Funktion zur Verarbeitung eines Klicks auf den AN/AUS Schalter
// Parameter: event (Klick)
// Quelle: https://www.w3schools.com/jsref/event_onclick.asp
function handleManualSwitchClick(event) {
    const switchButton = event.target;
    const switchId = switchButton.id;
    const areaId = parseInt(switchId.slice(-1));
    const areaElement = document.getElementById(`imageField${areaId}`);

    if (switchButton.classList.contains('on')) {
        switchButton.classList.remove('on');
        switchButton.textContent = 'OFF';
        switchButton.classList.add('off');
        updateManualModeAreaStatus(areaId, 'OFF'); // Bereich auf OFF setzen
        if (areaElement.classList.contains('selected-manuell')) {
            areaElement.classList.remove('selected-manuell');
            areaElement.classList.add('selected-manuelloff');
        }
    } else {
        switchButton.classList.remove('off');
        switchButton.classList.add('on');
        switchButton.textContent = 'ON';
        updateManualModeAreaStatus(areaId, 'ON'); // Bereich auf ON setzen
        if (areaElement.classList.contains('selected-manuelloff')) {
            areaElement.classList.remove('selected-manuelloff');
            areaElement.classList.add('selected-manuell');
        }
    }

    // Aktualisieren Sie modeInfo basierend auf den Schalterzustaenden
    //Quelle: https://www.w3schools.com/jsref/prop_element_classlist.asp
    const switch1 = document.getElementById('manualSwitch1');
    const switch2 = document.getElementById('manualSwitch2');
    const switch3 = document.getElementById('manualSwitch3');

    modeInfo.manualArea1 = switch1.classList.contains('on') ? 'ON' : 'OFF';
    modeInfo.manualArea2 = switch2.classList.contains('on') ? 'ON' : 'OFF';
    modeInfo.manualArea3 = switch3.classList.contains('on') ? 'ON' : 'OFF';
    sendMessage(modeInfo);
    console.log(modeInfo);
}

// Event-Listener fuer Schalterklicks hinzufuegen
const manualSwitches = document.querySelectorAll('.manualSwitch');
manualSwitches.forEach(switchButton => {
    switchButton.addEventListener('click', handleManualSwitchClick);
});

// Funktion zum Aktualisieren der Sichtbarkeit der manuellen Modus Schalter
function updateManualModeSwitchesVisibility() {
    const manualSwitches = document.querySelector('.manualModeSwitches');
    const selectedImage = document.getElementById("imageField" + selectedImageNumber);
    // Verstecke alle Schalter zuerst
    document.querySelectorAll('.manualModeSwitch').forEach(switchButton => {
        switchButton.style.display = 'none';
    });
    if (modeInfo.manualMode) {
        manualSwitches.style.display = 'flex';
        const switchId = 'manualSwitch' + selectedImageNumber;
        const selectedSwitch = document.getElementById(switchId);
        if (selectedSwitch) {
            selectedSwitch.style.display = 'block';
        }
    } else {
        // Verstecke die Schalter, wenn kein manueller Modus ausgewaehlt ist
        manualSwitches.style.display = 'none';
    }
}
// Funktion zum Aktualisieren der Sichtbarkeit der manuellen Modus Schalter aufrufen
updateManualModeSwitchesVisibility();

// Funktion zum Umgang mit dem Fokus auf Textfeldern
// Parameter: textIs (ID des Textes)
function handleTextFocus(textId) {
    const input = document.getElementById(textId);
    // Ueberpruefen, ob der aktuelle Wert dem Standardwert entspricht
    if (input.value === "SS:MM" || input.value === "ON/OFF") {
        input.value = "";
    }
}

// Funktion zum Umgang mit dem Fokusverlust von Textfeldern
// Parameter: textIs (ID des Textes)
function handleTextBlur(textId) {
    const input = document.getElementById(textId);
    // Ueberpruefen, ob das Textfeld leer ist
    if (input.value === "") {
        // Den Standardwert entsprechend dem Textfeldtyp setzen
        input.value = textId.includes("manualText") ? "ON/OFF" : "SS:MM";
    }
}

//Phase 3 Zusatzaufgabe (teilweise bearbeitet): Individueller Bildupload aus Dateien in Bewaesserungsbereiche per Doppelklick
// Quelle Bildupload: https://stackoverflow.com/questions/22087076/how-to-make-a-simple-image-upload-using-javascript-html
function handleImageUpload() {
    // Das Eingabefeld fuer Dateien abrufen
    const input = document.getElementById("fileInput");
    // Die eindeutige Bildnummer aus dem Dataset abrufen
    const fieldNumber = input.dataset.fieldNumber;
    // Das Bildfeld entsprechend der Bildnummer abrufen
    const field = document.getElementById("imageField" + fieldNumber);
    // Die ausgewaehlte Datei aus dem Datei-Input abrufen
    const file = input.files[0];

    // Ueberpruefen, ob eine Datei ausgewaehlt wurde
    if (file) {
        // Ueberpruefen, ob die ausgewaehlte Datei ein Bild ist
        if (file.type.startsWith("image/")) {
            // Einen FileReader erstellen, um die Datei zu lesen
            const reader = new FileReader();
            // Event-Handler fuer das Laden der Datei festlegen
            reader.onload = function (e) {
                // Das Hintergrundbild des Bildfelds setzen
                field.style.backgroundImage = "url(" + e.target.result + ")";

                // Das ausgewaehlte Bild in der globalen Variable speichern
                selectedImages[fieldNumber] = e.target.result;
            };
            // Die Datei als Data URL einlesen
            reader.readAsDataURL(file);
        } else {
            // Warnung anzeigen, wenn keine Bilddatei ausgewaehlt wurde
            alert("Bitte waehlen Sie eine Bilddatei aus.");
        }
    }
}

// Funktion zum Umgang mit der Enter-Taste in einem Textfeld
// Parameter: event (Enter-Druck)
// Quelle Entertaste-Events: https://www.w3schools.com/howto/howto_js_trigger_button_enter.asp
// Quelle Regex E-Mail: https://stackoverflow.com/questions/46155/how-can-i-validate-an-email-address-in-javascript
function handleEnterKeyPress(event) {
    if (event.key === "Enter") {
        let userInput = event.target.value;
        let emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (emailRegex.test(userInput)) {
            console.log("Valid e-mail address:", userInput);
            mailAddress.emailAddress = userInput;
            sendMail();
            console.log(mailAddress);
            // Zuruecksetzen des roten Designs, wenn die Eingabe korrekt ist
            event.target.classList.remove("invalid-input");
        } else {
            console.log("Invalid e-mail address. Please enter a valid email address.");
            // Hinzufuegen des roten Designs, wenn die Eingabe ungueltig ist
            event.target.classList.add("invalid-input");
        }
    }
}

// Textfeld abrufen
let textField = document.getElementById("userInput");
// Event-Listener fuer die Enter-Taste hinzufuegen
textField.addEventListener("keyup", handleEnterKeyPress);
// Aktualisiert das HTML-Element mit der aktuellen Datum- und Uhrzeitinformation.
function updateDateTime() {
    let dateTimeElement = document.getElementById("dateTime");
    let now = new Date();
    let formattedDateTime = now.toLocaleString();
    dateTimeElement.innerHTML = formattedDateTime;
}

// Socket-Ereignis fuer empfangene Nachrichten
// Quelle Try:https://www.w3schools.com/js/js_errors.asp
socket.addEventListener("message", function (event) {
    // Speichern des Temperaturwerts in der temperatureVal-Struktur
    try {
        // Nachricht als JSON parsen
        const data = JSON.parse(event.data);
        if (data.hasOwnProperty("temperatureVal")) {
            temperatureVal.temperatureVal = data.temperatureVal;
        }
        // Ueberpruefen, ob das "errorIs"-Feld vorhanden ist und den erwarteten Werten entspricht
        if (data.hasOwnProperty("errorIs")) {
            if (data.errorIs === "true") {
                modeInfo.errorIs = "true";
                showError();
                console.log("Attention, error");
            } else if (data.errorIs === "false") {
                console.log("Everything OK");
                hideError();
            } else {
                console.log("Invalid value for errorIs:", data.errorIs);
            }
        }
        // Ueberpruefen, ob die Felder "area1", "area2" und "area3" vorhanden sind
        if (data.hasOwnProperty("area1") && data.hasOwnProperty("area2") && data.hasOwnProperty("area3")) {
            // Bereichfelder fuer Bewaesserungsbereiche abrufen
            let watherArea1 = document.getElementById("imageField1");
            let watherArea2 = document.getElementById("imageField2");
            let watherArea3 = document.getElementById("imageField3");
            // Fallunterscheidungen fuer verschiedene Kombinationen von Bereichen
            if (data.area1 === "Winter" && data.area2 === "Winter" && data.area3 === "Winter") {
                updateManualModeSwitchesVisibility();
                hideAutomaticModeTextFields();
                /* console.log("All in winter"); */
                console.log("Kein Bereich bewaessert");
                [watherArea1, watherArea2, watherArea3].forEach((area) => {
                    area.classList.remove("selected-winter", "selected-automatik", "selected-manuell", "selected-manuelloff");
                });
                modeInfo.area1 = modeInfo.area2 = modeInfo.area3 = "Winter";
                [watherArea1, watherArea2, watherArea3].forEach((area) => area.classList.add("selected-winter"));
            } else if (data.area1 === "Automatik" && data.area2 === "Automatik" && data.area3 === "Automatik") {
                updateManualModeSwitchesVisibility();
                hideAutomaticModeTextFields();
                /* console.log("All in automatic"); */
                [watherArea1, watherArea2, watherArea3].forEach((area) => {
                    area.classList.remove("selected-winter", "selected-automatik", "selected-manuell", "selected-manuelloff");
                });
                modeInfo.area1 = modeInfo.area2 = modeInfo.area3 = "Automatik";
                [watherArea1, watherArea2, watherArea3].forEach((area) => area.classList.add("selected-automatik"));
                isCurrentTimeInRange(modeInfo);
            } else if (data.area1 === "Manuell" && data.area2 === "Manuell" && data.area3 === "Manuell") {
                updateManualModeSwitchesVisibility();
                hideAutomaticModeTextFields();
                // Ueberpruefen, ob die Felder "manualArea1", "manualArea2" und "manualArea3" vorhanden sind
                if (data.hasOwnProperty("manualArea1") && data.hasOwnProperty("manualArea2") && data.hasOwnProperty("manualArea3")) {
                    // Fallunterscheidungen fuer verschiedene Kombinationen von manuellen Bereichen
                    if (data.manualArea1 === "ON" && data.manualArea2 === "ON" && data.manualArea3 === "ON") {
                        [watherArea1, watherArea2, watherArea3].forEach((area) => {
                            area.classList.remove("selected-winter", "selected-automatik", "selected-manuell", "selected-manuelloff");
                        });
                        modeInfo.area1 = modeInfo.area2 = modeInfo.area3 = "Manuell";
                        [watherArea1, watherArea2, watherArea3].forEach((area) => area.classList.add("selected-manuell"));
                        modeInfo.manualArea1 = "ON";
                        modeInfo.manualArea2 = "ON";
                        modeInfo.manualArea3 = "ON";
                        /* console.log("All areas irrigated"); */
                    } else if (data.manualArea1 === "OFF" && data.manualArea2 === "OFF" && data.manualArea3 === "OFF") {
                        [watherArea1, watherArea2, watherArea3].forEach((area) => {
                            area.classList.remove("selected-winter", "selected-automatik", "selected-manuell", "selected-manuelloff");
                        });
                        [watherArea1, watherArea2, watherArea3].forEach((area) => area.classList.add("selected-manuelloff"));
                        modeInfo.area1 = modeInfo.area2 = modeInfo.area3 = "Manuell";
                        modeInfo.manualArea1 = "OFF";
                        modeInfo.manualArea2 = "OFF";
                        modeInfo.manualArea3 = "OFF";
                        /* console.log("No area irrigated"); */
                    }
                }
            }
        }
    } catch (error) {
    }
});

// Funktion zum Anzeigen des Fehlers und Blockieren der Interaktion
// Quelle: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors?retiredLocale=de
function showError() {
    // Error-Div erstellen oder abrufen
    let errorDiv = document.getElementById("errorDiv");
    if (!errorDiv) {
        errorDiv = document.createElement("div");
        errorDiv.id = "errorDiv";
        errorDiv.textContent = "Error!!!";
        errorDiv.classList.add("errorStyle");
        // Zusaetzliche Nachricht hinzufuegen
        let errorMessage = document.createElement("div");
        errorMessage.textContent = "(Prüfen Sie die Bewässerung)";
        errorMessage.classList.add("errorMessage");
        errorDiv.appendChild(errorMessage);
        document.body.appendChild(errorDiv);
        // Benutzerinteraktion blockieren
        document.body.style.pointerEvents = "none";
    }
}

// Funktion zum Entfernen des Fehlers und Wiederherstellen der Interaktion
function hideError() {
    let errorDiv = document.getElementById("errorDiv");
    if (errorDiv) {
        errorDiv.parentNode.removeChild(errorDiv);
        // Benutzerinteraktion wiederherstellen
        document.body.style.pointerEvents = "auto";
        modeInfo.errorIs = "false";
    }
}

// ueberpruefung von errorIs und Anzeige des Fehlers bei Bedarf
if (modeInfo.errorIs === "true") {
    showError();
} else {
    hideError();
}

// Funktion zum Aktualisieren des Dashboard mit aktuellen Bewaesserungsinformationen
function updateDashboard() {
    const areaStatus1 = document.getElementById("areaStatus1");
    const areaStatus2 = document.getElementById("areaStatus2");
    const areaStatus3 = document.getElementById("areaStatus3");

    // Ueberpruefen, ob ein Fehler vorliegt
    if (modeInfo.errorIs === "true") {
        areaStatus1.innerHTML = "<span style='color: red;'>B1: Nicht bewässert</span>";
        areaStatus2.innerHTML = "<span style='color: red;'>B2: Nicht bewässert</span>";
        areaStatus3.innerHTML = "<span style='color: red;'>B3: Nicht bewässert</span>";
        return;
    }
    // Ueberpruefen und aktualisieren des Status fuer Bereich 1
    if ((modeInfo.area1 === "Automatik" &&
        isCurrentTimeInRange(modeInfo.automaticFrom1, modeInfo.automaticTo1)) ||
        (modeInfo.area1 === "Manuell" && modeInfo.manualArea1 === "ON")) {
        areaStatus1.innerHTML = "<span style='color: green;'>B1: Bewässert</span>";
    } else {
        areaStatus1.innerHTML = "<span style='color: red;'>B1: Nicht bewässert</span>";
    }
    // Ueberpruefen und aktualisieren des Status fuer Bereich 2
    if ((modeInfo.area2 === "Automatik" &&
        isCurrentTimeInRange(modeInfo.automaticFrom2, modeInfo.automaticTo2)) ||
        (modeInfo.area2 === "Manuell" && modeInfo.manualArea2 === "ON")) {
        areaStatus2.innerHTML = "<span style='color: green;'>B2: Bewässert</span>";
    } else {
        areaStatus2.innerHTML = "<span style='color: red;'>B2: Nicht bewässert</span>";
    }
    // Ueberpruefen und aktualisieren des Status fuer Bereich 3
    if ((modeInfo.area3 === "Automatik" &&
        isCurrentTimeInRange(modeInfo.automaticFrom3, modeInfo.automaticTo3)) ||
        (modeInfo.area3 === "Manuell" && modeInfo.manualArea3 === "ON")) {
        areaStatus3.innerHTML = "<span style='color: green;'>B3: Bewässert</span>";
    } else {
        areaStatus3.innerHTML = "<span style='color: red;'>B3: Nicht bewässert</span>";
    }
}

// Funktion zum Ueberpruefen, ob die aktuelle Zeit im angegebenen Zeitraum liegt
// Funktion vereinfacht durch ChatGPT
function isCurrentTimeInRange(fromTime, toTime) {
    const currentTime = new Date();
    const from = new Date(currentTime.toDateString() + " " + fromTime);
    const to = new Date(currentTime.toDateString() + " " + toTime);
    return currentTime >= from && currentTime <= to;
}

// Aktualisieren des Dashboards beim Laden der Seite
updateDashboard(); 

// Aktualisieren des Dashboards in einem Intervall, um aenderungen in modeInfo zu reflektieren
setInterval(updateDashboard, 1000); // Alle Sekunde aktualisieren

// Funktion zum Senden des aktuellen Modus an den Server
const sendMessage = () => {
    socket.send(JSON.stringify(modeInfo));
}

// Funktion zum Aktualisieren des Temperaturwerts
function updateTemperatureValue() {
    // Waehle das <span>-Element aus
    let temperatureValueElement = document.getElementById("temperatureValue");

    // Aktualisiere den Text des <span>-Elements mit dem Wert von temperatureVal
    temperatureValueElement.textContent = "Temp: " + temperatureVal.temperatureVal + "°C";
}

// Aktualisiere den Temperaturwert zu Beginn und dann alle Sekunde
updateTemperatureValue(); // Erste Aktualisierung
setInterval(updateTemperatureValue, 1000); // Alle Sekunde aktualisieren

// Funktion zum Senden der E-Mail-Adresse an den Server
const sendMail = () => {
    socket.send(JSON.stringify(mailAddress));
}

// Aktualisierung der Datum/Uhrzeit-Anzeige im Intervall von 1000 Millisekunden (1 Sekunde)
// Quelle Zeitintervall: https://www.w3schools.com/jsref/met_win_setinterval.asp
setInterval(updateDateTime, 1000);
