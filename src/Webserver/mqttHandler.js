// Autor: Fabiola Streitenberger
// Klasse zum Verbinden und Subscriben zum MQTT-Broker und zum
// publishen von Nachrichten

// Anpassungen mit Chat GPT

class MQTTClient {

    constructor(client, logic, subscribedTopics, wsClient) {
        this.client = client;
        this.subscribedTopics = subscribedTopics;
        this.connected = false;
        this.logic = logic;
        this.wsClient = wsClient;
    }

    // Methode, um nach Initialisierung vom Websocket diesen zu
    // uebergeben
    getWebSocket(webSocketClient) {
        this.webSocketClient = webSocketClient;
    }

    // Methode, um zu einem Array von Topics zu subscriben
    connect() {
        this.client.on('connect', () => {
            console.log('Connected to MQTT broker');
            this.connected = true;
            this.subscribedTopics.forEach(topic => {
                this.client.subscribe(topic, (err) => {
                    if (err) {
                        console.error('Error subscribing to topic:', topic);
                    } else {
                        console.log('Subscribed to topic:', topic);
                    }
                });
            });
        });
        const wsClient = this.wsClient;
        // Pruefen, ob fuer die auszufuehrende Logik webSocketClient benoetigt wird
        if (wsClient === 0) {
            this.client.on('message', (topic, message, ) => {
            this.logic.handleMessage(topic, message)
        });
        } else {
            this.client.on('message', (topic, message) => {
            this.logic.handleMessage(topic, message, this.webSocketClient, this.client)
            });
        }
    }

    // Methode, um eine Nachricht zu einem Topic zu publishen
    publish(topic, message) {
        if (!this.connected) {
            console.error('Cannot publish, not connected to MQTT broker');
            return;
        }

        this.client.publish(topic, message, (err) => {
            if (err) {
                console.error('Error publishing message:', err);
            } else {
                console.log(`Published message '${message}' to topic '${topic}'`);
            }
        });
    }
}

module.exports = {MQTTClient};