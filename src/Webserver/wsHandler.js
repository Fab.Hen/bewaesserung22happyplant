// Autor: Fabiola Streitenberger
// Klasse zum Senden und Empfangen von Nachrichten
// ueber Websocket

const webServerHandler = require("./wsMessageHandler");

// Anpassungen mit Chat GPT

class WebSocketClient {

    constructor(WebSocketServer, clients, webSocket, mqttClient, wsMessageHandler) {
        this.WebSocketServer = WebSocketServer;
        this.clients = clients;
        this.webSocket = webSocket;
        this.mqttClient = mqttClient;
        this.wsMessageHandler = wsMessageHandler;
    }

    // Nachricht ueber Websocket empfangen und entsprechendes Objekt,
    // das die auszufueherenden Funktionen erhaelt aufrufen
    onConnection() {
        this.WebSocketServer.on("connection", (ws) => {
            console.log("A new client Connected!");
            this.clients.add(ws);
            ws.send("Welcome New Client!");
            ws.on("message", (message) => {
                console.log(`Received new Message on Websocket: ${message}`);
                const mqttClients = this.mqttClient;
                this.wsMessageHandler.logic(mqttClients, message);
            });
        });
    }

    // Nachricht ueber Websocket versenden
    sendMessageToClient(modeInfo) {
        const message = JSON.stringify(modeInfo);
        const WebSocket = this.webSocket;
        this.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
            client.send(message);
            }
        });
    }
}

module.exports = {WebSocketClient};