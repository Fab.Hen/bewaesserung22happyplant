// Autor: Fabiola Streitenberger
// Objekt mit verschiedenen Funktionen, die je nach erhaltener
// Nachricht aufgerufen werden

const validator = require("validator");
const path = require('path');
const {handleFiles} = require("./configHandler");
const {webServerHandler} = require("./webServerHandler");

const mailConfigPath = path.join(__dirname, '..', 'mailConfig.json');
const mailHandler = new handleFiles();
const mailConfig = mailHandler.readConfig(mailConfigPath);


const wsMessageHandler = {
    logic(mqttClients, message) {
        const newData = JSON.parse(message);
        const mqttClient = mqttClients;
        if (newData.emailAddress) {
            this.handleMail(newData);
        } else if (newData.area1 || newData.area2 || newData.area3) {
            this.handleMode(message, newData, mqttClient);
        } else {
            this.handleDefault(message);
        }
    },

    handleMail(newData) {
        console.log('Handling type1 message');
        // Quelle Mail Validierung:
        // https://www.npmjs.com/package/validator
        if (validator.isEmail(newData.emailAddress) === true) {
            // angepasst mit ChatGPT
            // Wenn message E-Mail-Adresse enthaelt, mailOptions ergaenzen
            if (!mailConfig) {
                console.log('Error reading configuration files. Exiting...');
                process.exit(1); // Exit the process with a non-zero status code
            }
            mailConfig.toMailAddress += "," + newData.emailAddress;
            const addedAddresses = mailConfig.toMailAddress;
            mailHandler.writeConfig(addedAddresses, mailConfigPath);
            console.log(`Received email address: ${newData.emailAddress}`);
        } else {
            console.log("Input is no mail-address");
        }
    },

    handleMode(message, newData, mqttClient) {
        console.log('Handling type2 message');
        mqttClient.publish(webServerHandler.mqttPubTopic, message);
        webServerHandler.modeInfo = newData;
        console.log(`Published to MQTT: ${webServerHandler.modeInfo}`);

    },

    handleDefault(message) {
        console.log('Handling default message');
        console.log(`Unrecognized message format: ${message}`);
    }
}

module.exports = {wsMessageHandler};