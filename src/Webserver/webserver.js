// Autor: Fabiola Streitenberger
// Webserver, der zum MQTT-Broker subscribed und published
// und ueber Websocket mit der Webanwendung kommuniziert
// und Frostwarnung per Mail an User verwendet

const express = require("express");
const http = require("http");
const WebSocket = require("ws");
const mqtt = require("mqtt");
const path = require('path');
const {handleFiles} = require("./configHandler");
const {MQTTClient} = require("./mqttHandler");
const {webServerHandler} = require("./webServerHandler");
const {wsMessageHandler} = require("./wsMessageHandler");
const {WebSocketClient} = require("./wsHandler");


// Webserver Setup
const app = express();
const webServer = http.createServer(app);
// Quelle WebSocketServer:
// https://github.com/Vuka951/tutorial-code/tree/master/js-express-websockets
// WebsocketServer upgraded den http webServer
const WebSocketServer = new WebSocket.Server({server: webServer});
const clients = new Set();
app.use(express.static("html"));


// Quelle options:
// https://cedalo.com/blog/nodejs-mqtt-usage-with-mosquitto-example/
const options = {
    clientId: "userID",
    username: "username",
    password: "password"
};


// MQTT Setup
const mqttUrl = "mqtt://localhost";
const mqttClient = mqtt.connect(mqttUrl, options);
// MQTT Topics, die abonniert werden sollen
const mqttSubTopic = ["temperature", "joystickMode", "error"];
// MQTT Topics, zu denen veroeffentlicht werden soll
const mqttPubTopic = "mode";

// Objekt der Klasse MQTTClient initalisieren
const ws = 1;
const mqttWebClient = new MQTTClient(mqttClient, webServerHandler, mqttSubTopic, ws);
// Objekt der Klasse WebSocketClient initalisieren
const wsHandler = new WebSocketClient(WebSocketServer, clients, WebSocket, mqttWebClient, wsMessageHandler);

// Objekt wsHandler nachtraeglich an mqttWebClient uebergeben
wsHandler.onConnection();
mqttWebClient.getWebSocket(wsHandler);
mqttWebClient.connect();

// Portnummer des Webservers aus config.json lesen
const configPath = path.join(__dirname, '..', 'config.json');
const configHandler = new handleFiles();
const config = configHandler.readConfig(configPath);
const portNumber = config.webSocketPort;

// Webserver Setup auf Port 3000
webServer.listen(portNumber, () => {
    console.log(
        `Server has been started. Address = http://localhost:${portNumber}`
    );
});