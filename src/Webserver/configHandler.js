// Autor: Fabiola Streitenberger
// Klasse zum Lesen verschiedener Dateien und zum Schreiben in mailConfig

// Angelehnt an:
// https://blog.openreplay.com/how-to-read-and-write-json-in-javascript/
// Modifikationen mit ChatGPT
const fs = require("fs");

class handleFiles {
    constructor() {

    }
    readConfig(filename) {
        try {
            const data = fs.readFileSync(filename, 'utf8');
            return JSON.parse(data);
        } catch (err) {
            console.error('Error reading configuration file:', err);
            return null;
        }
    }
    writeConfig(data, filename) {
        try {
            const mailData = {
                toMailAddress: data
            }
            const jsonData = JSON.stringify(mailData);
            fs.writeFileSync(filename, jsonData, 'utf8');
            console.log('Configuration file updated successfully.');
        } catch (err) {
            console.error('Error writing configuration file:', err);
        }
    }
}

module.exports = {handleFiles};