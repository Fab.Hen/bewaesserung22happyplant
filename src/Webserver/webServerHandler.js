// Autor: Fabiola Streitenberger
// Objekt, das die Funktionen enthaelt, die abhaengig von MQTT Topic und Dateninhalt
// ausgefuehrt werden 

const {mail} = require("./mailHandler");


const webServerHandler = {
    oldTemperature: 15,

    mqttPubTopic: "mode",

    modeInfo: {
        area1: "Winter",
        area2: "Manuell",
        area3: "Manuell",
        automaticFrom1: "12:00",
        automaticTo1: "15:00",
        automaticFrom2: "12:00",
        automaticTo2: "15:00",
        automaticFrom3: "12:00",
        automaticTo3: "15:00",
        manualArea1: "OFF",
        manualArea2: "OFF",
        manualArea3: "OFF",
        errorIs: "false"
    },

    // Pruefen, zu welchem Topic Nachricht empfangen wurde
    handleMessage(topic, message, wsClient, mqttClient) {
        switch (topic) {
            case "temperature":
                this.temperature(message, wsClient, mqttClient);
                break;
            case "joystickMode":
                this.joystickMode(message, wsClient, mqttClient);
                break;
            case "error":
                this.error(message, wsClient, mqttClient);
                break;
            default:
                break;
        }
    },

    // Funktion, die aufgerufen wird, wenn Nachricht zum Topic temperature empfangen wird
    temperature(message, wsClient, mqttClient) {
        // message in Objekt umwandeln
        let jsonData = JSON.parse(message.toString());
        // Pruefen fuer Bereich 1, ob Wintermodus verlassen werden muss
        if (jsonData.temperatureVal > 15 && this.modeInfo.area1 === "Winter") {
            this.modeInfo.area1 = "Automatik";
            wsClient.sendMessageToClient(this.modeInfo);
            mqttClient.publish(this.mqttPubTopic, JSON.stringify(this.modeInfo));
        }
        // Pruefen fuer Bereich 2, ob Wintermodus verlassen werden muss
        if (jsonData.temperatureVal > 15 && this.modeInfo.area2 === "Winter") {
            this.modeInfo.area2 = "Automatik";
            wsClient.sendMessageToClient(this.modeInfo);
            mqttClient.publish(this.mqttPubTopic, JSON.stringify(this.modeInfo));
        }
        // Pruefen fuer Bereich 3, ob Wintermodus verlassen werden muss
        if (jsonData.temperatureVal > 15 && this.modeInfo.area3 === "Winter") {
            this.modeInfo.area3 = "Automatik";
            wsClient.sendMessageToClient(this.modeInfo);
            mqttClient.publish(this.mqttPubTopic, JSON.stringify(this.modeInfo));
        }
        // Pruefen, ob Frostwarnung versendet werden muss
        if (jsonData.temperatureVal < 5 && this.oldTemperature > 5) {
            // Mail versenden
            // Quelle Mail-Funktionen:
            // https://www.w3schools.com/nodejs/nodejs_email.asp
            mail.transporter.sendMail(mail.mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log(`Email sent: ${info.response}`);
                }
            });
        }
        // aktuellen Temperaturwert an oldTemperatur uebergeben
        this.oldTemperature = jsonData.temperatureVal;
        // Temperatur an Websocket senden
        wsClient.sendMessageToClient(jsonData);
    },

    // Funktion, die aufgerufen wird, wenn Nachricht zum Topic joystickMode empfangen wird
    joystickMode(message, wsClient, mqttClient) {
        // message in Objekt umwandeln
        let jsonData = JSON.parse(message.toString());
        // Alle Bereiche in Wintermodus versetzten
        if (jsonData.joystickInput === "w") {
            this.modeInfo.area1 = "Winter";
            this.modeInfo.area2 = "Winter";
            this.modeInfo.area3 = "Winter";
        } else if (jsonData.joystickInput === "a") {
            // Alle Bereiche in Automatikmodus versetzten
            this.modeInfo.area1 = "Automatik";
            this.modeInfo.area2 = "Automatik";
            this.modeInfo.area3 = "Automatik";
        } else if (jsonData.joystickInput === "d") {
            // Alle Bereiche in manuellen Modus versetzten, Bewaesserung aus
            this.modeInfo.area1 = "Manuell";
            this.modeInfo.area2 = "Manuell";
            this.modeInfo.area3 = "Manuell";
            this.modeInfo.manualArea1 = "OFF";
            this.modeInfo.manualArea2 = "OFF";
            this.modeInfo.manualArea3 = "OFF";
        } else if (jsonData.joystickInput === "s") {
            // Alle Bereiche in manuellen Modus versetzten, Bewaesserung an
            this.modeInfo.area1 = "Manuell";
            this.modeInfo.area2 = "Manuell";
            this.modeInfo.area3 = "Manuell";
            this.modeInfo.manualArea1 = "ON";
            this.modeInfo.manualArea2 = "ON";
            this.modeInfo.manualArea3 = "ON";
        } else {
        }
        // Modi an Websocket senden und ueber MQTT publishen
        wsClient.sendMessageToClient(this.modeInfo);
        mqttClient.publish(this.mqttPubTopic, JSON.stringify(this.modeInfo));
    },

    // Funktion, die aufgerufen wird, wenn Nachricht zum Topic error empfangen wird
    error(message, wsClient, mqttClient) {
        // message in String umwandeln
        let status = message.toString();
        // Pruefen, ob Error und angepasstes modeInfo an Websocket senden
        console.log(status);
        if (status === "0") {
            this.modeInfo.errorIs = "false";
            wsClient.sendMessageToClient(this.modeInfo);
            mqttClient.publish(this.mqttPubTopic, JSON.stringify(this.modeInfo));
        } else {
            this.modeInfo.errorIs = "true";
            wsClient.sendMessageToClient(this.modeInfo);
            mqttClient.publish(this.mqttPubTopic, JSON.stringify(this.modeInfo));
        }
    }
}

module.exports = {webServerHandler};