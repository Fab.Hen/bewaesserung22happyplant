// Autor: Fabiola Streitenberger
// Enthaelt Daten, die zum Versenden der Frostwarnung per
// E-Mail notwendig sind

const nodemailer = require("nodemailer");
const {handleFiles} = require("./configHandler");
const path = require('path');

// Quelle Pfadaufruf:
// ChatGPT
const configPath = path.join(__dirname, '..', 'config.json');
const configHandler = new handleFiles();
const config = configHandler.readConfig(configPath);

const mailConfigPath = path.join(__dirname, '..', 'mailConfig.json');
const mailHandler = new handleFiles();
const mailConfig = mailHandler.readConfig(mailConfigPath);


// Ueberpruefen, ob sich Dateien Lesen lassen
// Chat GPT
if (!config || !mailConfig) {
    console.error('Error reading configuration files. Exiting...');
    process.exit(1); // Exit the process with a non-zero status code
}

// Quelle Mail Funktionen:
// https://www.w3schools.com/nodejs/nodejs_email.asp
const mail = {
    transporter: nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: config.fromMailAddress,
            pass: config.mailPassword
        }
    }),
    mailOptions: {
        from: config.fromMailAddress,
        to: mailConfig.toMailAddress,
        subject: "Frostwarnung!",
        text: "Es ist kälter als 5°C! Schützen Sie bitte Ihre Pflanzen vor Frost."
    }
};

module.exports = {mail};