// Autor: Fabiola Streitenberger
// abhaengig von welchem MQTT Topic eine Nachricht empfangen wird, werden an
// unterschiedliche Bereiche der Cloudanwendung Temperaturdaten oder
// Bewaesserungszustand gesendet

const axios = require("axios");
const path = require('path');
const {handleFiles} = require("./configHandler");

const configPath = path.join(__dirname, '..', 'config.json');
const configHandler = new handleFiles();
const config = configHandler.readConfig(configPath);

const api = config.api;

const cloudServiceHandler = {
    handleMessage(topic, message) {
        switch (topic) {
            case "area1":
                this.area1(message);
                break;
            case "area2":
                this.area2(message);
                break;
            case "area3":
                this.area3(message);
                break;
            case "temperature":
                this.temperature(message);
                break;
            default:
                break;
        }
    },
    area1(message) {
        const url = `https://api.thingspeak.com/update?api_key=${api}&field1=${message}`;
        this.sendMode(url);
    },
    area2(message) {
        const url = `https://api.thingspeak.com/update?api_key=${api}&field2=${message}`;
        this.sendMode(url);
    },
    area3(message) {
        const url = `https://api.thingspeak.com/update?api_key=${api}&field3=${message}`;
        this.sendMode(url);
    },
    temperature(message) {
        let jsonData = JSON.parse(message.toString());
        const tempValue = jsonData.temperatureVal;
        console.log(`Temperatur für cloud: ${tempValue}`);
        const url = `https://api.thingspeak.com/update?api_key=${api}&field4=${tempValue}`;
        this.sendMode(url);
    },
    sendMode(url) {
        // ChatGPT
        axios.get(url)
            .then(response => {
                console.log(response.data);
            })
            .catch(error => {
                console.error("Error making HTTP request:", error);
            });
    }
}

module.exports = { cloudServiceHandler };