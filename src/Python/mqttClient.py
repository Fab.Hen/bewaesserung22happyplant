# Autor: Fabian Hennegriff
"""mqttClient Klasse

Wird in dieser Version nur zum publishen Verwendet,
Ziel ist es auch mit ihr zu subscriben.

"""
import json
import os
import paho.mqtt.client as mqtt  # Klasse für die MQTT-Verbindung


class MqttClient:
    def __init__(self):
        """
        Initialisiert einen MQTT-Client.

        Verbindungseinstellungen zum MQTT-Broker werden festgelegt,
        und die Verbindung wird hergestellt. Die MQTT-Schleife wird gestartet.
        Fuer Phase 3 soll auch das subscriben zu einer topic mit
        dieser Klasse moeglich sein.

        """
        self.client = mqtt.Client()
        self.client.onConnect = self.onConnect
        self.client.on_message = self.onMessage

        # MQTT-Broker Verbindung
        #self.brokerAddress = "localhost" 
        #self.brokerPort = 1883
        
        configFilePath = os.path.join(os.path.dirname(__file__), "..", "config.json")

        # Laden der config.json
        with open(configFilePath, "r") as file:
            configData = json.load(file)
            self.brokerAddress = configData["mqttBroker"]
            self.brokerPort = int(configData["mqttPort"])

        self.client.connect(self.brokerAddress, self.brokerPort, 60)

        # Start die MQTT-Schleife
        self.client.loop_start()
        
        self.latestMessage = None

    def onConnect(self, client, userdata, flags, rc):
        """
        Wird bei erfolgreicher Verbindung zum MQTT-Broker aufgerufen.

        Parameter:
        - client: Der MQTT-Client.
        - userdata: Benutzerdaten fuer die MQTT-Verbindung.
        - flags: Flags für die Verbindung.
        - rc: Rückgabecode, 0 bedeutet erfolgreiche Verbindung.

        """
        if rc == 0:
            print("Verbindung zum MQTT-Broker aufgebaut")
    
    def onMessage(self, client, userdata, msg):
        """
        Wird bei Empfang einer Nachricht aufgerufen.

        Parameter:
        - client: Der MQTT-Client.
        - userdata: Benutzerdaten fuer die MQTT-Verbindung.
        - msg: Die empfangene Nachricht.

        """
        #print(f"Nachricht erhalten auf Topic: {msg.topic}. Inhalt: {msg.payload}")
        self.latestMessage = msg.payload.decode()

    def pubMQTT(self, topic, message):
        """Veroeffentlicht eine MQTT-Nachricht.

        Parameter:
        - topic: Das MQTT-Topic, auf das die Nachricht veroeffentlicht wird.
        - message: Die zu veroeffentlichende Nachricht.

        """
        self.client.publish(topic, message)
    
    def subMQTT(self, topic):
        """Abonniert ein MQTT-Topic.

        Parameter:
        - topic: Das MQTT-Topic, das abonniert werden soll.

        """
        self.client.subscribe(topic)
        print(f"Abonniere Topic: {topic}")
