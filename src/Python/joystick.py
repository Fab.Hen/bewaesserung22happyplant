# Autor: Fabian Hennegriff

"""Programm welches den Joystick simuliert

Wartet auf Benutzer-Eingabe und sendet diese weiter
es sollten nur eingaben wie wasd gemacht werden
da es sich nur um einen Mock handelt, wurde nicht extra eine Eingabepruefung eingebaut 
   
"""

import time
import json

# Importiere die SenseHat-Klasse
from sense_hat import SenseHat

# Importiere die MqttClient-Klasse
from mqttClient import MqttClient  

# Erstellt ein SenseHat-Objekt
senseHat = SenseHat()

# Erstellt ein MqttClient-Objekt
mqttClient = MqttClient()

# Loescht das Display
senseHat.clear()

print("Joystick started")
while True:
    for event in senseHat.stick.get_events():
        if event.action == "pressed":
            directionMap = {
                "up": "w",
                "down": "s",
                "left": "a",
                "right": "d",
            }

            direction = event.direction
            # zeigt Eingabe auf SenseHat-Display an
            if direction in directionMap:
                # senseHat.show_letter(directionMap[direction])
                # Published die Joystick Eingabe zur Topic joystickMode im JSON-Format
                payload = {"joystickInput": directionMap[direction]}
                payloadJson = json.dumps(payload)
                mqttClient.pubMQTT("joystickMode", payloadJson)
            else:
                time.sleep(1)
""" Joystick-Modus-Simulation
while True:
    position = "d"  # Standard-Joystick-Position (kann später durch Benutzereingabe aktualisiert werden)
    position = input()
    payload = {"joystickInput": position}  # Erstellen einer Payload
    payloadJson = json.dumps(payload)  # Konvertiere die Payload in JSON
    mqttClient.pubMQTT(
        "joystickMode", payloadJson
    )  # Sende das JSON an den MQTT-Broker mit der topic "joystickInput"
    time.sleep(1)  # Warten für 1 Sekunde, bevor die naechste Eingabe moeglich ist
"""
