# Autor: Fabian Hennegriff

"""Programm, das Temperaturwerte in die Datenbank schreibt.

Zum Subscriben zu einer Mqtt-topic wurde
https://pypi.org/project/paho-mqtt/#usage-and-api als Quelle verwendet

"""


import json
import time
import os

# importiere die databaseHandler-Klasse
from databaseHandler import databaseHandler

# Importiere die MqttClient-Klasse
from mqttClient import MqttClient


# Laden der config.json
configFilePath = os.path.join(os.path.dirname(__file__), "..", "config.json")

with open(configFilePath, "r") as file:
    configData = json.load(file)

host = configData["databaseHost"]
port = configData["databasePort"]
user = configData["databaseUser"]
password = configData["databasePassword"]
database = configData["database"]

mqttTopic = "temperature"


# Erstellt ein databaseHandler-Objekt
databaseHandler = databaseHandler(host, port, user, password, database)

# erstellt ein MqttClient-Objekt
mqttTemperatureClient = MqttClient()

input = None

while True:
    # Subscribed zu der MQTT-Topic "temperature"
    mqttTemperatureClient.subMQTT(mqttTopic)

    latestMessage = mqttTemperatureClient.latestMessage

    # Wenn eine Nachricht erhalten wurde, wird diese in die Datenbank eingefuegt
    if latestMessage != None:
        temperature = json.loads(latestMessage)
        input = temperature.get("temperatureVal", "")
        # prueft ob die Nachricht eine float ist
        if isinstance(input, float):
            # print("latestMessage ist eine float")
            databaseHandler.connect()
            databaseHandler.insertTemperature(input)
            databaseHandler.close()
        else:
            print("Error: latestMessage is not a float")
    else:
        print("No message received.")

    measuringInterval = float(configData["measuringInterval"])
    time.sleep(measuringInterval)
