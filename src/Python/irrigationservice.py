# Autor: Fabian Hennegriff

"""Programm, welches die Bewaesserungsbereiche verwaltet

Subscribed zur mqtt-topic "mode".
Gibt den aktuellen Modus aus, 
und steuert die Bewaesserungsbereiche je nach Modus.
Prueft und verarbeitet die Zeitangaben fuer den Automatikmodus.

Zum Subscriben zu einer Mqtt-topic wurde
https://pypi.org/project/paho-mqtt/#usage-and-api als Quelle verwendet

"""

import json
import time
import datetime
from mqttClient import MqttClient  # Importiere die MqttClient-Klasse

# Erstellt MqttClient-Objekte
mqttModeClient = MqttClient()
mqttESPClient = MqttClient()


# Hier hat ChatGPT sehr geholfen, modeInfo muss vor
# der onMessage Funktion einmal initialisiert werden
modeInfo = {
    "area1": "Manuell",
    "area2": "Manuell",
    "area3": "Manuell",
    "automaticFrom1": "12:20",
    "automaticTo1": "16:21",
    "automaticFrom2": "12:20",
    "automaticTo2": "12:21",
    "automaticFrom3": "12:20",
    "automaticTo3": "12:21",
    "manualArea1": "ON",
    "manualArea2": "ON",
    "manualArea3": "ON",
    "errorIs": "false",
}


def checkTime(time1, time2):
    """Prueft ob die aktuelle Zeit zwischen time1 und time2 liegt

    Parameters:
    - time1: Der erste Zeitwert im Format HH:MM.
    - time2: Der zweite Zeitwert im Format HH:MM.

    Returns:
    - 1 wenn die aktuelle Zeit zwischen time1 und time2 liegt
    - 0 wenn die aktuelle Zeit nicht zwischen time1 und time2 liegt
    """
    currentTime = datetime.datetime.now().strftime("%H:%M")
    if time1 > time2:
        return 20
    elif time1 <= currentTime <= time2:
        return 1
    else:
        return 0


def irrigationManagement(modeInfo):
    """Verwaltet den BewaesserungsModus

    Prueft zuerst ob es einen Error gibt.
    Gibt dann je nach Bereich und Modus
    den jeweiligen Bewaesserungszustand aus
    und Publisht 0 oder 1 an die Topic des jeweiligen Bereichs


    Parameter:
    - modeInfo: Der aktuelle Temperaturwert
    """
    for i in range(1, 4):
        areaKey = f"area{i}"
        manualKey = f"manualArea{i}"

        # Prueft ob im Error Zustand
        if modeInfo.get("errorIs", "") == "true":
            print("Error, all areas off")
            mqttESPClient.pubMQTT(f"area1", "0")
            mqttESPClient.pubMQTT(f"area2", "0")
            mqttESPClient.pubMQTT(f"area3", "0")
        # Prueft den Modus
        elif modeInfo[areaKey] == "Winter":
            # print(f"Area {i} off, Winter mode active")
            mqttESPClient.pubMQTT(f"area{i}", "0")
        elif modeInfo[areaKey] == "Automatik":
            automaticFromKey = f"automaticFrom{i}"
            automaticToKey = f"automaticTo{i}"
            if checkTime(modeInfo[automaticFromKey], modeInfo[automaticToKey]) == 1:
                # print(f"Area {i}  on")
                mqttESPClient.pubMQTT(f"area{i}", "1")
            elif checkTime(modeInfo[automaticFromKey], modeInfo[automaticToKey]) == 0:
                # print(f"Area {i} off")
                mqttModeClient.pubMQTT(f"area{i}", "0")
            elif checkTime(modeInfo[automaticFromKey], modeInfo[automaticToKey]) == 20:
                print(f"Wrong time format for area {i}")
        elif modeInfo[areaKey] == "Manuell":
            if modeInfo[manualKey] == "OFF":
                # print(f"Area {i} off")
                mqttESPClient.pubMQTT(f"area{i}", "0")
            elif modeInfo[manualKey] == "ON":
                # print(f"Area {i} on")
                mqttESPClient.pubMQTT(f"area{i}", "1")


while True:
    # Pause um Packet zu Verarbeiten
    time.sleep(0.5)  
    mqttModeClient.subMQTT("mode")
    
    # Prueft ob eine Nachricht vorhanden ist
    if mqttModeClient.latestMessage is not None:
        modeInfo = json.loads(mqttModeClient.latestMessage)
    
    # Aufruf der irrigationManagement-Funktion
    irrigationManagement(modeInfo)  
