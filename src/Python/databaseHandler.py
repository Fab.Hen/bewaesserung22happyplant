# Autor: Fabian Hennegriff

""" Datenbanklasse
DataBaseHandler, der die Verbindung zur Datenbank herstellt 
und die Temperaturwerte in die Datenbank schreibt.
"""

# importiere die mysql.connector-Klasse
import mysql.connector
from mysql.connector import errorcode


class databaseHandler:
    """Konstruktor der Datenbankklase
    Um eine Instanz zu erstellen werden Folgende Parameter benoetigt:
    - host: der Datenbank Host, hier localhost andernfalls eine IP
    - port: der Datenbank Port
    - user: der Datenbanknutzer, welcher fuer Queries verwendet wird
    - password: das Nutzerpasswort
    - databse: der Name der datenBank auf die zugregriffen wird 
    """
    def __init__(self, host, port, user, password, database):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database
        self.connection = None

    def connect(self):
        """Verbindet sich mit der Datenbank"""
        try:
            self.connection = mysql.connector.connect(
                host=self.host,
                port=self.port,
                user=self.user,
                password=self.password,
                database=self.database,
            )
            print(f"Connected to database: {self.database}")
        except mysql.connector.Error as err:
            print(f"Error: {err}")
            self.connection = None

    def insertTemperature(self, temperature):
        """Fuegt die erhaltene Temperatur in die Datenbank ein"""
        try:
            cursor = self.connection.cursor()
            insertQuery = "INSERT INTO temperatureLog (temperature) VALUES (%s)"
            data = (temperature,)

            # Das Query wird ausgefuehrt und die Daten werden in die Datenbank eingefuegt
            cursor.execute(insertQuery, data)
            self.connection.commit()
            print(f"Temperature {temperature} inserted into database.")
        except mysql.connector.Error as err:
            # Wenn es ein Problem beim Verbinden gibt, wird ein Fehler ausgegeben
            # Code wurde von
            # https://dev.mysql.com/doc/connector-python/en/connector-python-example-connecting.html
            # kopiert
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Wrong User/Password.")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist.")
            else:
                print(err)

    def close(self):
        """Schliesst die Datenbankverbindung"""
        if self.connection:
            try:
                self.connection.close()
                print("Database connection closed.")
            except mysql.connector.Error as err:
                print(f"Error: {err}")
