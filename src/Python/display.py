# Autor: Fabian Hennegriff

"""Programm welches die Sense-HAT Anzeige verwaltet

Subscribed zu den topics "mode" und "temperature"

Zum Subscriben zu einer Mqtt-topic wurde
https://pypi.org/project/paho-mqtt/#usage-and-api als Quelle verwendet

"""


import time
import os
import json

# Importiere die MqttClient-Klasse
from mqttClient import MqttClient  

# Importiere die SenseHat-Klasse
from sense_hat import SenseHat

# Erstellt MqttClient-Objekte
mqttTemperatureClient = MqttClient()
mqttModeClient = MqttClient()

# Erstellt ein SenseHat-Objekt
senseHat = SenseHat()

# Laden der config.json
configFilePath = os.path.join(os.path.dirname(__file__), "..", "config.json")

with open(configFilePath, "r") as file:
    configData = json.load(file)
# Laden des Messintervalls aus der config datei
measuringInterval = float(configData["measuringInterval"])

# Standartwerte fuer modeInfo
modeInfo = {
    "area1": "Manuell",
    "area2": "Manuell",
    "area3": "Manuell",
    "automaticFrom1": "12.20",
    "automaticTo1": "12.21",
    "automaticFrom2": "12.20",
    "automaticTo2": "12.21",
    "automaticFrom3": "12.20",
    "automaticTo3": "12.21",
    "manualArea1": "OFF",
    "manualArea2": "OFF",
    "manualArea3": "OFF",
    "errorIs": "false",
}

# Standarttemperatur
temperature = {"temperatureVal": "25.0"}


def colorManagement(area, color):
    """Setzt die Farbe der Pixel auf der Sense-HAT
    Wird fuer die Anzeige der Bewaesserungsbereiche verwendet,
    wird in der displayManagement-Funktion aufgerufen.
    Paramerter:
        area : der Bereich, in dem die Farbe gesetzt werden soll
        color: die Farbe, die gesetzt werden soll
    """
    width = 8
    height = 8
    if area == 1:
        startCol = 0
        endCol = 2
    elif area == 2:
        startCol = 3
        endCol = 5
    elif area == 3:
        startCol = 6
        endCol = 8
    for col in range(startCol, endCol):
        for row in range(height):
            senseHat.set_pixel(col, row, *color)


def displayManagement(modeInfo):
    """Verwaltet den BewaesserungsModus

    Prueft zuerst ob es einen Error gibt.
    Gibt dann je nach Bereich und Modus
    den jeweiligen Bewaesserungszustand aus


    Parameter:
    - modeInfo: Der aktuelle Modus"""
    for i in range(1, 4):
        areaKey = f"area{i}"
        manualKey = f"manualArea{i}"
        # Prueft ob im Error Zustand
        if modeInfo.get("errorIs", "") == "true":
            print("Error, all areas off")
            # Rot
            senseHat.clear((255, 0, 0))
        # Prueft ob ein Bereich im Wintermodus ist
        elif modeInfo[areaKey] == "Winter":
            # print(f"Area {i} off, wintermode")
            # Tuerkis
            colorManagement(i, (86, 180, 233))

        # Prueft ob ein Bereich im Automatikmodus ist
        elif modeInfo[areaKey] == "Automatik":
            # print(f"Area {i} , automatic mode")
            # Gruen
            colorManagement(i, (0, 153, 0))

        # Prueft ob ein Bereich im Manuellen Modus ist
        elif modeInfo[areaKey] == "Manuell":
            if modeInfo[manualKey] == "OFF":
                # print(f"Area {i} off, manual mode")
                # Dunkel Violett
                colorManagement(i, (127, 0, 125))

            elif modeInfo[manualKey] == "ON":
                # print(f"Area {i} on manual mode")
                # Gelb
                colorManagement(i, (230, 159, 0))


while True:
    # Subscribed zu den topics "mode" und "temperature"
    mqttTemperatureClient.subMQTT("temperature")
    mqttModeClient.subMQTT("mode")

    # Zeigt die Temperatur auf der Sense-HAT an
    senseHat.show_message(f"{temperature['temperatureVal']}C")

    # Ruft die displayManagement-Funktion auf
    if mqttModeClient.latestMessage != None:
        modeInfo = mqttModeClient.latestMessage
        displayManagement(json.loads(modeInfo))
    else:
        print("No modeInfo received")

    time.sleep(measuringInterval)

    #
    if mqttTemperatureClient.latestMessage != None:
        temperature = json.loads(mqttTemperatureClient.latestMessage)
    else:
        print("No temperature received")
