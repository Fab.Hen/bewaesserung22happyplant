# Autor: Fabian Hennegriff

"""Sensor der Temperperatur-Werte published

Published zur MQTT-topic temperature in einem in der config.json festgelegten Intervall.
Misst die aktuelle Temperatur am SenseHat und published diese in einem JSON-Format.

"""

# import random
import time
import json
import os

# Importiere die MqttClient-Klasse
from mqttClient import MqttClient

# Importiere die SenseHat-Klasse
from sense_hat import SenseHat

# Erstellt ein MqttClient-Objekt
mqttClient = MqttClient()

# Erstellt ein SenseHat-Objekt
senseHat = SenseHat()


# Laden der config.json
configFilePath = os.path.join(os.path.dirname(__file__), "..", "config.json")

with open(configFilePath, "r") as file:
    configData = json.load(file)
# Laden des Messintervalls aus der config datei
measuringInterval = float(configData["measuringInterval"])


def readTemperature(temperatureVal):
    """misst die aktuelle Temperatur am SenseHat
    Parameter:
    - temperatureVal: Der aktuelle Temperaturwert
    """
    #   temperatureVal += random.uniform(-1.0, 1.0)
    temperatureVal = senseHat.get_temperature()
    return round(temperatureVal, 2)


# Initialer Wert, um Funktionalitaet von Frostwarnungs-E-mail
# und automatischem Moduswechsel zu testen
temperatureVal = 24.0

print("Sensor started")
while True:
    # Misst die aktuelle Temperatur und published diese
    temperatureVal = readTemperature(temperatureVal)
    payload = {"temperatureVal": temperatureVal}
    payloadJson = json.dumps(payload)  # Konvertiere die Payload in JSON
    mqttClient.pubMQTT("temperature", payloadJson)

    time.sleep(measuringInterval)  # Intervall In dem gemessen wird
