// Autor: Fabian Hennegriff
// esp.cpp-Datei , die die Methoden der Esp-Klasse definiert
#include "esp.h"

esp::esp(/* args */)
{
}

void esp::setupWifi()
{
    // Methode zum Verbinden mit dem WLAN, wird in setup() aufgerufen

    // Verbindung mit dem WLAN herstellen
    WiFi.begin(userSSID, userPassword);

    // Erstellen eines neuen WiFiClients
    espClient = new WiFiClient(); // Erstellen eines neuen WiFiClients

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.println("Connecting to WiFi..");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void esp::callback(char *topic, byte *payload, unsigned int length)
{
    // Methode zum Empfangen von MQTT Nachrichten, wird im setup() bei setCallback() uebergeben
    // char *topic: Topic der Nachricht
    // byte *payload: Nachricht
    // unsigned int length: Laenge der Nachricht

    Serial.print(topic);
    // Prueft ob die Nachricht von einem der drei Topics kommt
    // und setzt den entsprechenden Pin auf HIGH oder LOW
    if (String(topic) == "area1")
    {
        if ((char)payload[0] == '1')
        {
            digitalWrite(LEDPin1, HIGH);
            Serial.println("on");
        }
        else if ((char)payload[0] == '0')
        {
            digitalWrite(LEDPin1, LOW);
            Serial.println("off");
        }
    }
    else if (String(topic) == "area2")
    {
        if ((char)payload[0] == '1')
        {
            digitalWrite(LEDPin2, HIGH);
            Serial.println("on");
        }
        else if ((char)payload[0] == '0')
        {
            digitalWrite(LEDPin2, LOW);
            Serial.println("off");
        }
    }
    else if (String(topic) == "area3")
    {
        if ((char)payload[0] == '1')
        {
            digitalWrite(LEDPin3, HIGH);
            Serial.println("on");
        }
        else if ((char)payload[0] == '0')
        {
            digitalWrite(LEDPin3, LOW);
            Serial.println("off");
        }
    }
    else
    {
        Serial.println("Error");
    }
}

void esp::reconnect()
{
    // Methode zum erneuten Verbinden mit dem MQTT Server
    while (!pubSubClient->connected())
    {
        Serial.print("Attempting MQTT connection...");
        // erstelle eine zufällige client ID
        String clientId = "ESP32Client-";
        clientId += String(random(0xffff), HEX);
        // Verbindungsversuch
        if (pubSubClient->connect(clientId.c_str()))
        {
            Serial.println("connected");
            pubSubClient->subscribe(subTopic1);
            pubSubClient->subscribe(subTopic2);
            pubSubClient->subscribe(subTopic3);
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(pubSubClient->state());
            Serial.println(" try again in 5 seconds");
            // neuer Versuch in 5 Sekunden
            delay(5000);
        }
    }
}

void esp::setup()
{
    // Setup Methode, die im setup() der main.cpp aufgerufen wird
    Serial.begin(115200);

    // setzen der Pinmodes
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(LEDPin1, OUTPUT);
    pinMode(LEDPin2, OUTPUT);
    pinMode(LEDPin3, OUTPUT);
    pinMode(BUTTON, INPUT_PULLUP);

    // aufrufen der Funktion zum Verbinden mit dem WLAN
    setupWifi();
    pubSubClient = new PubSubClient(*espClient);

    // MQTT
    pubSubClient->setServer(userIP, 1883);
    // Die der Funktion uebergebenen Werte wurden von ChatGPT hinzugefuegt
    pubSubClient->setCallback(std::bind(&esp::callback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void esp::loop()
{
    // Loop Methode, die im loop() der main.cpp aufgerufen wird

    // Verbindung zum MQTT Server aufrecht erhalten
    if (!pubSubClient->connected())
    {
        // Wenn keine Verbindung besteht, wird versucht eine neue Verbindung aufzubauen
        reconnect();
    }
    pubSubClient->loop();

    // Abfrage des Error Zustands und publishen je nach Zustand
    if (digitalRead(BUTTON) == HIGH)
    {
        pubSubClient->publish(pubTopicError, "0");
    }
    else
    {
        pubSubClient->publish(pubTopicError, "1");
    }

    delay(100);
}
