// Autor Fabian Hennegriff
#include "config.h"

// Configurationsdatei fuer WLAN und MQTT
// Ihre eigene SSID:
const char *userSSID = "<xxx>";
// Ihr WLAN Passwort:
const char *userPassword = "<12345678>";
// Die IP-Adresse ihres Raspberry Pi's:
const char *userIP = "<192.168.xx.xxx>";