// Autor: Fabian Hennegriff

// Fuer die mqtt-Verbindung wird die Bibliothek "PubSubClient" benoetigt
// Es wurde sich an den Beispielen der Bibliothek orientiert
// Quelle: https://github.com/knolleary/pubsubclient/pull/459/files

// Zum Beschalten der LEDS und des Buttons wurde:
// https://esp32io.com/tutorials/esp32-button-led als Vorlage genommen

#include "esp.h"

esp *espObject;

void setup()
{
  espObject = new esp();
  espObject->setup();
}

void loop()
{
  espObject->loop();
}
