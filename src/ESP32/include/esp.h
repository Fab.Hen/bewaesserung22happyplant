// Autor: Fabian Hennegriff
#ifndef esp_h
#define esp_h

#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_Sensor.h>
#include "config.h"

class esp
{
private:
    // Pinbelegung
    const int LEDPin1 = 5;
    const int LEDPin2 = 18;
    const int LEDPin3 = 19;
    const int BUTTON = 21;

    // MQTT Topics
    const char *subTopic1 = "area1";
    const char *subTopic2 = "area2";
    const char *subTopic3 = "area3";
    const char *pubTopicError = "error";

    // WiFi und MQTT Client als nullptr initialisieren
    WiFiClient *espClient = nullptr;
    PubSubClient *pubSubClient = nullptr;

    // Private Methoden
    // Methode zum Verbinden mit dem WLAN, wird in setup() aufgerufen
    void setupWifi(); 

    // Methode zum Empfangen von MQTT Nachrichten, wird im setup() bei setCallback() uebergeben
    void callback(char *topic, byte *payload, unsigned int length);

    // Methode zum Verbinden mit dem MQTT Broker, wird in setup() aufgerufen
    void reconnect();

public:
    esp(/* args */);
    ~esp();

    // Public Methoden

    // Methode zum Initialisieren der Pins und des MQTT Clients, wird in setup() aufgerufen
    void setup();

    // Loop Methode
    void loop();
};

#endif // esp_h