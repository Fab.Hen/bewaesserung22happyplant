// Autor Fabian Hennegriff
#ifndef config_h
#define config_h

extern const char *userSSID;
extern const char *userPassword;
extern const char *userIP;

#endif // config_h