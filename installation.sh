#!/bin/bash
sudo apt-get update 
sudo apt-get upgrade
sudo dpkg-reconfigure tzdata


curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm install node  
sudo apt install mariadb-server
sudo mysql_secure_installation

# Beispiel Datenbank-Nutzerdaten
dbUser="admindb"
dbPassword="1234" 

sudo mysql -uroot -p << EOF
CREATE DATABASE temperature;
CREATE USER '$dbUser'@'localhost' IDENTIFIED BY '$dbPassword';
GRANT ALL PRIVILEGES ON temperature.* TO '$dbUser'@'localhost';
FLUSH PRIVILEGES;
USE temperature;
CREATE TABLE temperatureLog(id INT AUTO_INCREMENT PRIMARY KEY,temperature FLOAT,timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
DELIMITER //
CREATE EVENT delete_old_records
ON SCHEDULE EVERY 1 DAY
DO
    DELETE FROM temperatureLog WHERE timestamp < NOW() - INTERVAL 4 WEEK;
//DELIMITER ;
EOF

sudo apt-get install sense-hat
sudo apt install python3-pip



echo "$PWD" 
cd src/Webserver
npm install package.json
cd ..
echo "$PWD"
cd Python
pip install -r requirements.txt



sudo apt install mosquitto
sudo apt install mosquitto mosquitto-clients
cd /etc/mosquitto 
echo "$PWD" 
sudo touch conf.d/local.conf 
sudo nano conf.d/local.conf 
sudo systemctl enable mosquitto
sudo systemctl start mosquitto

echo "Script zuende"
